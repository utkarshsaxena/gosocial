from django import forms
from LogIn.models import UserProfile, Ribbit
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.html import strip_tags

class UserForm(forms.ModelForm):
    first_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'First Name'}))
    last_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Last Name'}))
    username = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Enter a username'}))
    email = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'abc@example.com'}))
    password = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder': 'Password'}))    
    about_me = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Introduce yourself'}))
    class Meta:
        model = User
        fields = ('first_name','last_name','username','email','password','about_me')
        
class UserProfileForm(forms.ModelForm):
    Organization = (
        ('UBC', 'UBC'),
        ('SFU', 'SFU'),
        ('Others', 'Others'),
    )
    website = forms.URLField(help_text="Please enter your website.", required=False)
    picture = forms.ImageField(help_text="Select a profile image to upload.", required=False)
    group = forms.CharField(max_length=3,widget=forms.Select(choices=Organization))
	
    class Meta: 
        model = UserProfile
        fields = ('website', 'picture','group')

class RibbitForm(forms.ModelForm):
    content = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'class': 'ribbitText', 'placeholder': 'Enter Details'}))
    #location = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'class': 'ribbitLocation', 'placeholder': 'Enter Location'}))
    #date = forms.DateField(help_text='Format: 25 Oct 2006')
    #time = forms.TimeField(help_text='Format: 14:30') 
    def is_valid(self):
        form = super(RibbitForm, self).is_valid()
        for f in self.errors.iterkeys():
            if f != '__all__':
                self.fields[f].widget.attrs.update({'class': 'error ribbitText'})
        return form
 
    class Meta:
        model = Ribbit
        exclude = ('user',)
