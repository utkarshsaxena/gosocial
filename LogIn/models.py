from django.db import models
from django.contrib.auth.models import User, Group
import hashlib

# Create your models here.
Organization = (
        ('UBC', 'UBC'),
        ('SFU', 'SFU'),
        ('Others', 'Others'),
    )

class Ribbit(models.Model):
    content = models.CharField(max_length=200)
    #date = models.DateField(blank=False)
    #time = models.TimeField(blank=False)
    #location = models.CharField(max_length=40)
    user = models.ForeignKey(User)
    creation_date = models.DateTimeField(auto_now=True, blank=True)

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    about_me = models.CharField(max_length=140, null = True, blank = False)
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)
    follows = models.ManyToManyField('self', related_name='followed_by', symmetrical=True)
    group = models.CharField(max_length=3, choices=Organization)
    
    def gravatar_url(self):
        return "http://www.gravatar.com/avatar/%s?s=50" % hashlib.md5(self.user.email).hexdigest()

    def __unicode__(self):
        return self.user.username

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
