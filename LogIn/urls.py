from django.conf.urls import patterns, include, url
from LogIn import views
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', views.user_login, name='index'),
                       url(r'^register/$', views.register, name='register'),
                       url(r'^logout/$', views.user_logout, name='logout'),
                       url(r'profile/$',views.profile, name='profile'),
                       url(r'about/$',views.about, name='About Us'),
                       url(r'buddies/$',views.index1, name='Ribbit page'),
                       url(r'submit$',views.submit, name='Submit Ribbit'),
                       url(r'public/$',views.public, name='Public Ribbit'),
                       url(r'^users/$', views.users),
                       url(r'^users/(?P<username>\w{0,30})/$', views.users),
                       url(r'^follow$', views.follow),
                        
                       )

if settings.DEBUG:
        urlpatterns += patterns(
                'django.views.static',
                (r'media/(?P<path>.*)',
                'serve',
                {'document_root': settings.MEDIA_ROOT}), )
