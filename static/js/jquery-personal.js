$(document).ready(function() {	
	
	$('.carousel').carousel();
	$( '.carousel-caption' ).hide();
	$('.navbar').hide();
	$('#glychange1').hide();
	
	$('.lastName').hide();
	$('.userName').hide();
	$('.email').hide();
	$('.password').hide();
	$('.group').hide();
	$('.website').hide();
	$('.aboutMe').hide();
	$('.profilePicture').hide();
	$('.submit-button').hide();
	$('.previous').hide();
	
	
	
	
	$('#image1').hover(function() {
		$( "#caption1" ).show("2000");
	});
	
	$('#image2').hover(function() {
		$( "#caption2" ).show("2000");
	});
	
	$('#image3').hover(function() {
		$( "#caption3" ).show("2000");
	});
	
	$('.icon-menu').click(function() {
		$( "#glychange0" ).toggle("fast");
		$( "#glychange1" ).toggle("fast");
		$('.headArea').toggleClass("white-bg");
		$( ".navbar" ).toggle("8000");
	});
	
	$(".social-media-1").hover(function() {
	$('.social-media-1').css("background-color","rgba(59,89,152,0.5)");

	},function(){
	$('.social-media-1').css("background-color","rgba(8,188,188,0.6)");	
	
	});
	
	$(".social-media-1").click(function() {
	$('.social-media-1').children('h3').text('Thanks for connecting with us!');
	$('.social-media-1').children('h2').text(" :D ");	
	});
	
	////
	
	$(".social-media-2").hover(function() {
	$('.social-media-2').css("background-color","rgba(0,172,237,0.5)");

	},function(){
	$('.social-media-2').css("background-color","rgba(8,188,188,0.4)");	
	
	});
	
	$(".social-media-2").click(function() {
	$('.social-media-2').children('h3').text('Thanks for tweeting us!');
	$('.social-media-2').children('h2').text(" =D");	
	});
	
	////
	$(".social-media-3").hover(function() {
	$('.social-media-3').css("background-color","rgba(64,99,156,0.5)"); 

	},function(){
	$('.social-media-3').css("background-color","rgba(8,188,188,0.2)");	
	
	});
	
	$(".social-media-3").click(function() {
	$('.social-media-3').children('h3').text('Thanks for following us!');
	$('.social-media-3').children('h2').text(" =P ");	
	});
	
	
	$i = 0 ;
	$('.next').click(function() {
	$i = $i + 1 ;
	switch($i) {
	case 0:
		$('.previous').hide();
		$('.firstName').show();
		$('.lastName').hide();
		break;
	case 1:
		$('.firstName').hide();
		$('.userName').hide();
		$('.lastName').show();
		$('.previous').show();
		break;
	case 2:
		$('.lastName').hide();
		$('.email').hide();
		$('.userName').show();
		break;
	case 3:
		$('.userName').hide();
		$('.password').hide();
		$('.email').show();
		break;
	case 4:
		$('.email').hide();
		$('.website').hide();
		$('.password').show();
		break;
	case 5:
		$('.password').hide();
		$('.aboutMe').hide();
		$('.website').show();
		break;
	case 6:
		$('.website').hide();
		$('.profilePicture').hide();
		$('.aboutMe').show();
		break;
	case 7:
		$('.aboutMe').hide();
		$('.group').hide();
		$('.profilePicture').show();
		break;
	case 8:
		$('.profilePicture').hide();
		$('.submit-button').hide();
		$('.next').show();
		$('.group').show();		
		break;
	case 9:
		$('.group').hide();
		$('.submit-button').show();
		$('.next').hide();	
	}	
	});
	
	$('.previous').click(function() {
	$i = $i - 1 ;
	switch($i) {
	case 0:
		$('.previous').hide();
		$('.firstName').show();
		$('.lastName').hide();
		break;
	case 1:
		$('.firstName').hide();
		$('.userName').hide();
		$('.lastName').show();
		$('.previous').show();
		break;
	case 2:
		$('.lastName').hide();
		$('.email').hide();
		$('.userName').show();
		break;
	case 3:
		$('.userName').hide();
		$('.password').hide();
		$('.email').show();
		break;
	case 4:
		$('.email').hide();
		$('.website').hide();
		$('.password').show();
		break;
	case 5:
		$('.password').hide();
		$('.aboutMe').hide();
		$('.website').show();
		break;
	case 6:
		$('.website').hide();
		$('.profilePicture').hide();
		$('.aboutMe').show();
		break;
	case 7:
		$('.aboutMe').hide();
		$('.group').hide();
		$('.profilePicture').show();
		break;
	case 8:
		$('.profilePicture').hide();
		$('.submit-button').hide();
		$('.next').show();
		$('.group').show();		
		break;
	case 9:
		$('.group').hide();
		$('.submit-button').show();
		$('.next').hide();	
	}	
	});
	
	 $("#team").click(function(){
    $("#div1").load("/static/js/demo_test.txt");
  });

});
var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}
// [END region_geolocation]

function sync() {
		var hour = document.getElementById('hour');
		var minute = document.getElementById('minute');
		var event = document.getElementById('event_box');
		var description = document.getElementById('description_box');
		var street = document.getElementById('street_number');
		var route = document.getElementById('route');
		var locality = document.getElementById('locality');
		
		event.value = description.value + ' @ ' + hour.value + ':' + minute.value + ' on ' + street.value + ', ' + route.value + ', ' + locality.value;	
	}
	

