# README #

## Introduction:  

My first Django - A Python Web Framework application from scratch.  - **GoSocial**
It is a twitter-based-website which allows you to create and send out spontaneous/micro events to all your followers on the go!

## Technologies: ##

* Bootstrap
* Python
* Python Web Framework - Django

## Purpose: ##

* Learn and apply Django framework to create an application.
* Learn and apply Bootstrap to design the homepage of the application.

## Procedure: ##

1. Install Python ( https://www.python.org/ )
2. Download Django - A Web Framework for Python ( https://www.djangoproject.com/ )
3. Open the Command Prompt and execute the following command in the folder: **python manage.py runserver**



## Timestamp: ##

**August 2014 – November 2014**